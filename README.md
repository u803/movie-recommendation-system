# Movie Recommendation System

## Problem Statement
Recommender System is a system that filter preferences according to the user’s choices. Recommender systems are utilized in a variety of areas including movies, music, news, books, research articles, search queries, social tags, and products in general.

Content-based filtering approaches uses a series of discrete characteristics of an item in order to recommend additional items with similar properties. Content-based filtering methods are totally based on a description of the item and a profile of the user’s preferences. It recommends items based on the user’s past preferences.

Movie Recommendation System is based on **cosine similarity**. The main objective of the movie Recommendation System is to compute the similarity of different movies for the one the user needs and all the similar movies are recommended to user.

